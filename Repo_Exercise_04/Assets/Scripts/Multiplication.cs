﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Multiplication : MonoBehaviour
    {
        public int numberOne;
        public int numberTwo;
        public float sum;

        public void Multi()
        {
            float sum = numberOne * numberTwo;

            Debug.Log("Summe:" + sum);
        }

        public void AnotherMulti()
        {
            int numberThree = 2;
            int numberFour = 2;

            float sum = numberThree * numberFour;

            Debug.Log("Summe:" + sum);
        }

        public float Sum(int numberFive, int numberSix)
        {
            float sumTwo = numberFive * numberSix;

            return sumTwo;
        }

        public void Start()
        {
            Multi();
            AnotherMulti();
            sum = Sum(3, 3);

            Debug.Log("Summe:" + sum);
        }
    }
}
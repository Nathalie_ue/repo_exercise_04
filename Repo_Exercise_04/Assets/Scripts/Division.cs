﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Division : MonoBehaviour
    {
        public int numberOne;
        public int numberTwo;
        public float sum;

        public void Div()
        {
            float sum = numberOne / numberTwo;

            Debug.Log("Summe:" + sum);
        }

        public void AnotherDiv()
        {
            int numberThree = 2;
            int numberFour = 1;

            float sum = numberThree / numberFour;

            Debug.Log("Summe:" + sum);
        }

        public float Sum(int numberFive, int numberSix)
        {
            float sumTwo = numberFive / numberSix;

            return sumTwo;
        }

        public void Start()
        {
            Div();
            AnotherDiv();
            sum = Sum(4, 2);

            Debug.Log("Summe:" + sum);
        }
    }
}
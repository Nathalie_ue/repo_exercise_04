﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Addition : MonoBehaviour
    {
        public int numberOne;
        public int numberTwo;
        public float sum;

        public void Add()
        {
            float sum = numberOne + numberTwo;

            Debug.Log("Summe:" + sum);
        }
        
        public void AnotherAdd ()
        {
            int numberThree = 1;
            int numberFour = 2;

            float sum = numberThree + numberFour;

            Debug.Log("Summe:" + sum);
        }

        public float Sum (int numberFive, int numberSix)
        {
            float sumTwo = numberFive + numberSix;

            return sumTwo;
        }

        public void Start()
        {
            Add();
            AnotherAdd();
            sum = Sum(3,4);

            Debug.Log("Summe:" + sum);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Subtraction : MonoBehaviour
    {
        public int numberOne;
        public int numberTwo;
        public float sum;

        public void Sub()
        {
            float sum = numberOne - numberTwo;

            Debug.Log("Summe:" + sum);
        }

        public void AnotherSub()
        {
            int numberThree = 2;
            int numberFour = 1;

            float sum = numberThree - numberFour;

            Debug.Log("Summe:" + sum);
        }

        public float Sum(int numberFive, int numberSix)
        {
            float sumTwo = numberFive - numberSix;

            return sumTwo;
        }

        public void Start()
        {
            Sub();
            AnotherSub();
            sum = Sum(4, 3);

            Debug.Log("Summe:" + sum);
        }
    }
}